const nthFib = (n) => {
	let i = 1;
	let result = [1,1];
	while (i<n-1){
		result.push(result[i]+result[i-1]);
		i++;
	}
	return result[n-1];
};

export default nthFib;